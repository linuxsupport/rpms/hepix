# z2 / ALICE shell settings
## https://its.cern.ch/jira/browse/NOAFS-507
## based on /afs/cern.ch/alice/offline/etc/login.sh

# CASTOR environment
export STAGE_HOST=castoralice
export STAGE_SVCCLASS=t0alice

# Load ALICE's environment from CernVM-FS (Bash version)
hpx_source /cvmfs/alice.cern.ch/etc/login.sh
