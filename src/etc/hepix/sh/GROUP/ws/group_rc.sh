# based on /afs/cern.ch/group/ws/group_env.csh
# modified for migration to cvmfs
export OPAL_INSTALL_DIR="/cvmfs/opal.cern.ch"
if [ -f "${OPAL_INSTALL_DIR}/env/group_rc.sh" ]; then
    echo "Sourcing ${OPAL_INSTALL_DIR}/env/group_rc.sh"
    hpx_source ${OPAL_INSTALL_DIR}/env/group_rc.sh
else
    echo "${OPAL_INSTALL_DIR}/env/group_rc.sh is not available."
    echo "OPAL environment can not be set up."
fi
