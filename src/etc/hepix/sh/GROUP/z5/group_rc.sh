# https://its.cern.ch/jira/browse/NOAFS-508
# z5 / LHCb settings

# determine if this is a Grid pilot user
[[ "${HOME}" == "/home/grid/lhbplt"?? ]] && _is_pilot_user=1

if [ ! "${_is_pilot_user}" ]
then
    hpx_source /cvmfs/lhcb.cern.ch/lib/etc/cern_profile.sh
fi

unset _is_pilot_user
