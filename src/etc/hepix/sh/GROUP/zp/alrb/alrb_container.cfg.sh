# Unset local alrb mounts var
local_alrb_mounts=""
# make all of afs available
if [ -d /afs ]; then
    set_ALRB_ContainerEnv ALRB_CONT_RUNTIMEOPT "apptainer|-B /afs:/afs"
    set_ALRB_ContainerEnv ALRB_CONT_RUNTIMEOPT "podman|-v /afs:/afs"
    local_alrb_mounts="$local_alrb_mounts /afs"
fi

# make all of eos available
if [ -d /eos ]; then
    set_ALRB_ContainerEnv ALRB_CONT_RUNTIMEOPT "apptainer|-B /eos:/eos"
    set_ALRB_ContainerEnv ALRB_CONT_RUNTIMEOPT "podman|-v /eos:/eos"    
    local_alrb_mounts="$local_alrb_mounts /eos"
fi

if [ "$local_alrb_mounts" != "" ]; then
    set_ALRB_ContainerEnv ALRB_CONT_POSTSETUP "echo '$local_alrb_mounts available in this container'"
fi
unset local_alrb_mounts

# set HOME to be the same as the dir on lxplus instead of /home/<user>
# don't do this now as existing container users may be confused.
#set_ALRB_ContainerEnv ALRB_CONT_POSTSETUP "export HOME='$HOME'"
# instead provide a env for users to use if they want to cd to their real HOME
set_ALRB_ContainerEnv ALRB_CONT_POSTSETUP "export UHOME='$HOME'"

# use proxy caching for dockerhub
#  Dockerhub restricts the pull rates so this will aleviate that.
#  This setting is site specific; nearest proxy should be used.
export ALRB_CONT_PROXYREGISTRY="registry.cern.ch|docker.io,registry.hub.docker.com"
