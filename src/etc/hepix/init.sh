#!/bin/sh

[ -n "$_HPX_SEEN_CORE_INIT" ] && return
_HPX_SEEN_CORE_INIT=1

# Internal HEPiX variables.
HPX_INIT=""		# See end of this file.
HPX_HEPIX=""		# See hpx_is_hepix().
HPX_AFS_HOME=""		# See later.
HPX_UMASK=0022
HPX_HOME=/etc/hepix/sh	# Legacy code was in /usr/local/lib/hepix/.
HPX_HOME_CORE=$HPX_HOME/CORE
HPX_HOME_HEP=$HPX_HOME/HEP
HPX_HOME_GROUP=$HPX_HOME/GROUP

# Determine user HEPiX home directory.
[ -n "$HOME" -a -d "$HOME/.hepix" ] \
	&& HPX_USER_HOME=$HOME/.hepix \
	|| HPX_USER_HOME=""

# HEPiX off-button.
if [ -n "$HPX_USER_HOME" -a -r "${HPX_USER_HOME}/off" ]; then
	HPX_HEPIX=OFF
fi

# Determine verbosity level.
[ -n "$HPX_USER_HOME" -a -r "$HPX_USER_HOME/verbose-level" ] \
	&& HPX_VERBOSE=`/bin/cat $HPX_USER_HOME/verbose-level` \
	|| HPX_VERBOSE=0

# Determine debug level.
[ -n "$HPX_USER_HOME" -a -r "$HPX_USER_HOME/debug-level" ] \
	&& HPX_DEBUG=`/bin/cat $HPX_USER_HOME/debug-level` \
	|| HPX_DEBUG=0

# Determine if user's $HOME is on AFS.
echo $HOME | /bin/grep '^\/afs\/' > /dev/null && HPX_AFS_HOME=YES 

# Determine if we specifically want AFS for the group level scripts.
if [ -n "$HPX_USER_HOME" -a -r "$HPX_USER_HOME/wants-afs" ];then
  [ $HPX_VERBOSE -ge 11 ] && hpx_echo "N: $HPX_USER_HOME/wants-afs no longer has any effect"
fi

# Determine whether HEPiX is to be run.
hpx_is_hepix () {
	[ "$HPX_HEPIX" = OFF ] && return 1
	[ "$HPX_HEPIX" = YES ] && return 1
	[ "$USER" = "root" ] && return 1
        [ $UID -lt 100 ] && return 1
	HPX_HEPIX=YES
	return
}

hpx_source () {
	[ -z "$1" ] && return
	[ $HPX_VERBOSE -ge 13 ] && hpx_echo "N: CHECK $1"
	[ -r "$1" ] || return
	[ $HPX_VERBOSE -ge 11 ] && hpx_echo "N: -> SOURCE $1"
	. $1
	[ $HPX_VERBOSE -ge 12 ] && hpx_echo "N: PROCESSED $1"
}

hpx_echo () {
	[ -z "$1" ] && return
	echo "hepix: ${*}"
}

hpx_debug () {
	[ -z "$1" ] && return
	[ $HPX_DEBUG = 0 ] && return
	HPX_DATE=`/bin/date +'%Y-%m-%d %T'`
	hpx_echo "D: ${HPX_DATE} ${USER} "${SHELL}"["$$"] ("${SHLVL}"): ${*}"
        unset HPX_DATE
}

HPX_INIT=YES

hpx_debug "sh flavour init done"

# End of file.
