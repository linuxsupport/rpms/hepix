#!/bin/sh

[ -n "$_HPX_SEEN_ORACLE_ENV_SH" ] && return
_HPX_SEEN_ORACLE_ENV_SH=1

[ -z "$HPX_INIT" ] && . /etc/hepix/init.sh

ORACLE_CERN=/afs/cern.ch/project/oracle
export ORACLE_CERN
hpx_source $ORACLE_CERN/script/profile_oracle.sh

# End of file.
