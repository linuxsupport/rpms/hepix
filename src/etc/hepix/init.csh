#!/bin/csh

if ( $?_HPX_SEEN_CORE_INIT ) then
	exit
endif
set _HPX_SEEN_CORE_INIT=1

# Internal HEPiX variables.
set HPX_INIT=""		# See end of this file.
set HPX_HEPIX=""		# See hpx_is_hepix().
set HPX_AFS_HOME=""		# See later.
set HPX_UMASK=0022
set HPX_HOME=/etc/hepix/csh	# Legacy code was in /usr/local/lib/hepix/.
set HPX_HOME_CORE=$HPX_HOME/CORE
set HPX_HOME_HEP=$HPX_HOME/HEP
set HPX_HOME_GROUP=$HPX_HOME/GROUP

# Determine user HEPiX home directory.
if ( $?HOME ) then
	if ( -d $HOME/.hepix ) then
		set HPX_USER_HOME=$HOME/.hepix
	else
		set HPX_USER_HOME=""
	endif
endif

# HEPiX off-button.
if ( "x$?HPX_USER_HOME" != x ) then
	if ( -r $HPX_USER_HOME/off ) then
		set HPX_HEPIX=OFF
	endif
endif

# Determine verbosity level.
if ( "x$?HPX_USER_HOME" != x ) then
	if ( -r $HPX_USER_HOME/verbose-level ) then
		set HPX_VERBOSE=`/bin/cat $HPX_USER_HOME/verbose-level`
	else
		set HPX_VERBOSE=0
	endif
endif

# Determine debug level.
if ( "x$?HPX_USER_HOME" != x ) then
	if ( -r $HPX_USER_HOME/debug-level ) then
		set HPX_DEBUG=`/bin/cat $HPX_USER_HOME/debug-level`
	else
		set HPX_DEBUG=0
	endif
endif

# Determine if user's $HOME is on AFS.
echo $HOME | /bin/grep '^\/afs\/' > /dev/null
if ( $status == 0 ) then
	set HPX_AFS_HOME=YES
endif

# This is a Hack(tm), but a smart one nevertheless.  We are emulating shell
# functions (that are missing in csh(1) and derivatives) by using alias and
# script combinations.

alias hpx_is_hepix	"source $HPX_HOME_CORE/hpx_is_hepix.csh "
alias hpx_source 	"source $HPX_HOME_CORE/hpx_source.csh "
alias hpx_echo		"source $HPX_HOME_CORE/hpx_echo.csh "
alias hpx_debug		"source $HPX_HOME_CORE/hpx_debug.csh "

# Determine if we specifically wanted AFS for the group level scripts.
if ( "x$?HPX_USER_HOME" != x) then
  if ( -r $HPX_USER_HOME/wants-afs ) then
        if ( $HPX_VERBOSE >= 11 ) then
             hpx_echo "N: $HPX_USER_HOME/wants-afs no longer has any effect"
        endif
  endif
endif

set HPX_INIT=YES

hpx_debug "csh flavour init done"

# End of file.
