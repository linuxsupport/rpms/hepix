#!/bin/csh

if ( ! $#argv ) then
	exit
endif

# NOTE: This won't work in csh(1) since HPX_DEBUG will not be set in the
# executing shell.  Desol�e.  Inside the if statement we don't bother
# checking the variables' existence since in csh(1) we won't be executing
# that bit, and in tcsh(1) they are guaranteed to be defined anyway.
if ( $?HPX_DEBUG ) then
	if ( $HPX_DEBUG != 0 ) then
		set HPX_DATE=`/bin/date +'%Y-%m-%d %T'`
		hpx_echo "D: ${HPX_DATE} ${USER} "${SHELL}"["$$"] ("${SHLVL}"): ${*}"
		unset HPX_DATE
	endif
endif

# End of file.
