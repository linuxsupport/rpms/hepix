#!/bin/csh

if ( "$1" == "" ) then
	echo "hpx_path_append_unique(): Missing pathname."
	exit
endif

# Append given directory to PATH and remove all other
# occurrences.
# NOTE(fuji): Caller should eval() the returned result!

eval `hpx_path_remove "$1"`
eval `hpx_path_append "$1"`
echo "setenv PATH $PATH"

# End of file.
