#!/bin/csh

if ( ! $#argv ) then
	exit
endif

# volsize used %used %partition
set _HPX_LEFT=`/usr/bin/expr $1 - $2`
if ( $_HPX_LEFT <= 500 ) then
	hpx_echo "W: Your home directory has only ${_HPX_LEFT}KB left."
	hpx_echo "W: This may cause problems, especially when"
	hpx_echo "W: logging in.  Please remove unwanted files"
	hpx_echo "W: or go to http://cern.ch/account"
	hpx_echo "W: to increase your quota."
	exit
endif
if ( $3 >= 95 ) then
	hpx_echo "W: Your home directory is ${3}% full.  This may"
	hpx_echo "W: cause problems, especially when logging in."
	hpx_echo "W: Please remove unwanted files, or go to"
	hpx_echo "W: http://cern.ch/account to increase your quota."
	exit
endif
unset _HPX_LEFT

# End of file.
