# https://its.cern.ch/jira/browse/NOAFS-492
# environment for "t3" group
setenv PATH "/afs/cern.ch/project/parc/bin:$PATH"
setenv TEXINPUTS .:/afs/cern.ch/project/theory/tools/TeX/prosper//:
setenv WWW_HOME "http://wwwth.cern.ch"
