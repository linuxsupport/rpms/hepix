# NA48 / 'vl' shell variables
# https://its.cern.ch/jira/browse/NOAFS-496
# based on /afs/cern.ch/group/vl/group_env.csh

setenv STAGE_HOST castorpublic
setenv STAGE_SVCCLASS na62

setenv CDBHOME /afs/cern.ch/user/c/cdbase/public/CDBHOME
setenv CDSERV /afs/cern.ch/na48/offline/hepdb
setenv DBINCDIR /afs/cern.ch/na48/offline2/cdbase/code/inc
