# https://its.cern.ch/jira/browse/NOAFS-511
# zp / ATLAS settings, based on /afs/cern.ch/group/zp/group_env.csh

setenv PATH "/afs/cern.ch/atlas/scripts:$PATH"

setenv SITE_NAME CERN-PROD

setenv FRONTIER_SERVER '(serverurl=http://atlasfrontier-local.cern.ch:8000/atlr)(serverurl=http://atlasfrontier-ai.cern.ch:8000/atlr)(proxyurl=http://ca-proxy-atlas.cern.ch:3128)(proxyurl=http://ca-proxy-meyrin.cern.ch:3128)(proxyurl=http://ca-proxy.cern.ch:3128)(proxyurl=http://atlasbpfrontier.cern.ch:3127)'

if( -d /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase ) then 
   setenv ATLAS_LOCAL_ROOT_BASE /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
   alias setupATLAS 'source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.csh'
endif
