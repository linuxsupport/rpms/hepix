# z2 / ALICE shell settings
## https://its.cern.ch/jira/browse/NOAFS-507
## based on /afs/cern.ch/alice/offline/etc/login.csh

# Load ALICE's environment from CernVM-FS (C-Shell)
hpx_source /cvmfs/alice.cern.ch/etc/login.csh

# CASTOR environment
setenv STAGE_HOST castoralice
setenv STAGE_SVCCLASS t0alice
