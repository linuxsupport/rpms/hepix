# https://its.cern.ch/jira/browse/NOAFS-500
# settings for "wj" (NA61/SHINE)

if ( -d /cvmfs/na61.cern.ch ) then
    hpx_source /cvmfs/na61.cern.ch/etc/env.csh
endif
