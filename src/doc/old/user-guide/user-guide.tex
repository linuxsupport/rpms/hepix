\documentstyle[html,epsfig,rotating,longtable,12pt,here]{article}

\setlength{\textwidth}{170mm}
\setlength{\textheight}{240mm}
\setlength{\oddsidemargin}{1mm}
\setlength{\evensidemargin}{1mm}
\setlength{\marginparwidth}{1cm}
\voffset=-20mm
\hoffset=0mm
\setlength{\parindent}{0pt}

\makeatletter
\long\def\psboxit#1{%
\begingroup\setbox0\hbox{#1}%
\dimen0=\ht0 \advance\dimen0 by \dp0%
    % Write out the PS code to set the current path using HEIGHT,
    % WIDTH , DEPTH of box0.
    \hbox{%
    \special{ps:  /box{%                  Processes the path of a rectangle.
%                  Needs : x0 y0 x1 y1.
         newpath 2 copy moveto 3 copy pop exch lineto 4 copy pop pop
         lineto 4 copy exch pop exch pop lineto closepath } bind def
         gsave 0 setgray currentpoint translate
        0
        \number\dp0 \space 15800 div    % hand tuned for dvips
        \number\wd0 \space 15800 div    % hand tuned for dvips
        \number\ht0 \space -15800 div   % hand tuned for dvips
        box fill grestore gsave 1 setgray}%
       \copy0\special{ps: stroke grestore}%
}%HBOX
\endgroup
}%
\makeatother

%\newcommand{\percent}{\verb\ % \}
\newcommand{\prompt}{\verb+     +\psboxit{[plus1]} $\sim$\verb+ > +}
\newcommand{\tprompt}{\verb+    +\psboxit{[tcsh]} $\sim$\verb+ > +}
\newcommand{\zprompt}{\verb+    +\psboxit{[zsh]} $\sim$}
\newcommand{\return}{$\hookleftarrow$ }
\newcommand{\TAB}{{\it TAB}  }
\newcommand{\tcsh}{\verb+tcsh +}
\newcommand{\zsh}{\verb+zsh +}
\newcommand{\cursor}{\rule[-1mm]{2mm}{4mm} }
\newcommand{\ESC}{{\it ESC} }

\def\listfigurename{List of Examples}

\newcounter{ex}
%\newenvironment{Example}[1]{\begin{quote} \addtocounter{ex}{1}
%\underline{Example \arabic{ex}:} #1 \\ \\
% \addcontentsline{lof}{figure}{\protect\numberline{\arabic{ex}}{#1}}}{\end{quote}}


\newenvironment{Example}[1]{\addtocounter{ex}{1}
%\underline{\bf Example \arabic{ex}:} #1 \\ 
%\vspace{7mm}
\noindent
\addcontentsline{lof}{figure}{\protect\numberline{\arabic{ex}}{#1}}
\noindent 
\\}{}

\makeindex

\makeglossary


\begin{document}

\title{The HEPiX Shells and X11 Login Scripts Project\\
\vspace{1cm}
Implementation at CERN\\
\vspace{1cm}
User Guide\\
\vspace{1cm}
Version 3.0.5\\
\author{Arnaud Taddei \thanks{CERN - Arnaud.Taddei@cern.ch}}
     }
\begin{latexonly}
\maketitle
\end{latexonly}


\begin{htmlonly}
\begin{rawhtml}
<H1>The HEPiX Shells and X11 Login Scripts Project</H1>
<P>
<H1>Implementation at CERN</H1>
<P>
<H1>User Guide</H1>
<P>
Arnaud Taddei - Arnaud.Taddei@cern.ch
<HR>
\end{rawhtml}
\end{htmlonly}

\raisebox{110mm}[0mm][0mm]{\makebox[15cm][r]{CN/DCI/184}}

\begin{abstract}
This document describes how the user can customise the HEPiX scripts.
It gives some examples and provides references for further reading. 
\end{abstract}

\begin{htmlonly}
\begin{rawhtml}
<HR>
\end{rawhtml}
\end{htmlonly}

\newpage
\tableofcontents

\begin{htmlonly}
\begin{rawhtml}
<HR>
\end{rawhtml}
\end{htmlonly}

\section{Introduction}

This document describes how the user can customise the HEPiX scripts.\\
\\
In order to provide a common user environment on UNIX Systems 
a set of scripts was designed first by the DESY UNIX Committee (DUX)
and then by a joint project between DESY and CERN
(see \cite{taddei.hepix.ss.architect} which obsoletes 
\cite{taddei-koehler.hepixscripts}).\\
\\
These scripts set up the user environment which includes
the shell environment and the graphical environment (X11)
(in beta version now). The "startup scripts" (shell environment) 
provide a way for users to login, work in interactive and batch mode
for the most common shells and platforms. \\
\\
The following sections gives examples of what you can do to customise your 
environment, using features available via the HEPiX Shells and X11 login
scripts and for further reading you can access several related documents 
listed in the bibliography.\\

\begin{htmlonly}
\begin{rawhtml}
<HR>
\end{rawhtml}
\end{htmlonly}

\section{User customisations}

The user can customise his environment with his own dot files. The
following table gives the user files which are executed for each shell \\


\begin{table}[H]
{\bf \caption{Standard configuration files for the different shells}}
\label{standard-files}
\begin{center}
\begin{tabular}{||l|l|l||}
\hline
{\bf Shell}     & {\bf Conditions}      & {\bf Executed scripts} \\
\hline
csh             & always                & \$HOME/.cshrc         \\
                & login                 & \$HOME/.login         \\
\hline
tcsh            & always                & /etc/csh.cshrc        \\
                & login                 & /etc/csh.login or \$HOME/.login \\
                & interactive           & \$HOME/.tcshrc or \$HOME/.cshrc \\
\hline          
ksh             & always                & ENV variable if set  \\
                & login                 & /etc/profile or \$HOME/.profile \\
                & interactive           & \$HOME/.kshrc         \\
\hline
zsh             & always                & /etc/zshenv           \\
                &                       & \$HOME/.zshenv        \\
                & login                 & /etc/zprofile         \\
                &                       & \$HOME/.zprofile      \\
                &                       & \$HOME/.zlogin        \\
                & interactive           & /etc/zshrc            \\
                &                       & \$HOME/.zshrc         \\
\hline
bash            & always                & not possible          \\
                & login                 & /etc/profile          \\
                &                       & \$HOME/.bash\_profile or \$HOME/.profile \\
                & interactive           & \$HOME/.bashrc        \\
                & non-interactive       & ENV variable if set   \\
\hline
\end{tabular}
\end{center}
\end{table}

Note about {\bf tcsh}:\\
If you are using {\bf tcsh}   it is recommended that you have a 
{\tt .tcshrc} but no {\tt .cshrc}.\\

Concerning these 2 files, the behaviour of {\tt tcsh} is the following:
\begin{itemize}
\item if you have only {\tt .cshrc} (and no {\tt .tcshrc}), 
{\bf tcsh} uses {\tt .cshrc}
\item if you have only {\tt .tcshrc} (and no {\tt .cshrc}), 
{\bf tcsh} uses {\tt .tcshrc}
\item if you have both files, {\bf tcsh} uses only {\tt .tcshrc}
\end{itemize}


We will give some examples of tailoring the user environment to
personal taste.\\

\subsection{How can you specify your preferred group?}

Let's assume that you belong to several groups and sometimes you need
the environment of one group and in other circumstances you need the
environment of another group. In UNIX, there is the notion of 
primary group and once you logged in, your group {\it id} is set to 
that of the primary group. The HEPiX scripts provide you with a way of
specifying the
group name from which you want to get the environment using a file
{\verb+$HOME/.hepix/preferred-group+}. In this file you simply enter
the name of the group that you want.\\
\\
Practical example: I belong to groups c3, c4 and c5. On the system 
{\tt plus1}, my primary group is c3 but I want to inherit the environment
of group c5. Then, I simply edit the file {\verb+$HOME/.hepix/preferred-group+}
and I enter c5 in the first line.\\
\\
Thus, the next time that I login, I will get the environment of group c5.
\begin{quote}
For instance this mechanism doesn't allow you to specify that you would
like to inherit the environment from a group 1 on a service A and
from a group 2 on a service A. Thus when you set a preferred group
it is set for all the accounts on which you are using this home directory.
If for example you have an AFS account and you set a preferred group, then
it will be used on all the services on which you are using your AFS 
Home directory.
\end{quote}

\subsection{How can you change the behaviour of the RUBOUT sequence?}

There are many different keyboards available on the site. Keyboards are
sending signals and unfortunately, as there was no formal standard, it
exists some clashes on how to interpret this or that signal. The signal
which is sent to erase a character is called the RUBOUT sequence. 
Some keyboards may send \verb+^?+ which corresponds to a Delete and some
keyboards may send \verb+^H+ which corresponds to a BackSpace.\\
\\
Thus the HEPiX shells login scripts provide a environment variable named
RUBOUT which you can modify to 2 values : BackSpace or Delete.
\begin{itemize}
\item [Backspace] you need to create a \$HOME/.BackSpace file.
\item [Delete   ] you need to create a \$HOME/.Delete file.
\end{itemize}

In order to get a Backspace from the RUBOUT sequence enter the command:
\begin{verbatim}
        touch $HOME/.BackSpace
\end{verbatim}

In order to get a Delete from the RUBOUT sequence enter the command:
\begin{verbatim}
        touch $HOME/.Delete
\end{verbatim}

It will then be valid at the next login.

\subsection{How can you change your prompt?}

\begin{center}
\begin{table}[H]
\vspace{1cm}
\begin{tabular}{||l|l|l|l||}
\hline
{\bf Shell}       &{\bf File}           &{\bf example}                                  &{\bf Result} \\
\hline
csh               &.cshrc               &\verb+set prompt='\!>'+                        &\verb+24 >+\\
tcsh              &.tcshrc or .cshrc    &\verb+set prompt='\! %~ %#'+                   & \verb+24 ~ >+  \\
ksh               &.kshrc               &\verb+export PS1='\! >$'+                      & \verb+24 >+                   \\
zsh               &.zshrc               &\verb+export PS1="%S\[%T\]%s"%~ %# "+          & \psboxit{[23:55]} \verb+~ >+  \\
bash              &.bashrc              &\verb+export PS1='\${PWD} \! >'+               & \verb+ /bin 24 >+\\
\hline
\end{tabular}
\end{table}
\end{center}


If you want to include the current path in your C-Shell prompt then
you have to define the following alias for the {\tt cd} command.
\begin{verbatim}
alias cd chdir \!:\* \; set prompt='${cwd}\[!\]\ ' \; setenv CWD '$cwd'
\end{verbatim}

\subsection{How can you define your own aliases or functions?}

\begin{center}
\begin{table}[H]
\vspace{1cm}
\begin{tabular}{||l|l|l||}
\hline
{\bf Shell}       &{\bf File}           &{\bf example} \\
\hline
csh               &.cshrc               &\verb1alias rs 'set noglob;eval `resize`;unset noglob'1 \\
tcsh              &.tcshrc or .cshrc    &\verb1alias H 'history -r | fgrep "\!*"'1 \\
ksh               &.kshrc               &\verb1alias 'mr=chmod +r'1 \\
zsh               &.zshrc               &\verb1alias 'rs=eval `resize
-u`'1 \\
bash              &.bashrc              &\verb1mx() {chmod +x ${*}}1  \\
\hline
\end{tabular}
\end{table}
\end{center}

\subsection{How can you customise your PATH?}

Customising your {\tt PATH} is a fundamental issue which you should 
fully understand. Document \cite{taddei.dotinpath} gives interesting
input on this topic especially about the security issues when having a 
dot ({\tt .}) in the {\tt PATH}.

\begin{center}
\begin{table}[H]
\vspace{1cm}
\begin{tabular}{||l|l|l||}
\hline
{\bf Shell}       &{\bf File}           &{\bf example} \\
\hline
csh               &.cshrc               &set path=( \$path \$HOME/bin ) \\
tcsh              &.tcshrc or .cshrc    &set path=( \$path \$HOME/bin.\$OS ) \\
ksh               &.profile             &export PATH="\$HOME/bin:\$PATH"  \\
zsh               &.zprofile            &export PATH="\$HOME/bin.\$OS:\$PATH"\\
bash              &.bash\_profile or    &                    \\
                  & .profile            &export PATH="\$HOME/bin:\$PATH"  \\
\hline
\end{tabular}
\end{table}
\end{center}

\subsection{How can you set your own environment variables?}

\begin{center}
\begin{table}[H]
\vspace{1cm}
\begin{tabular}{||l|l|l||}
\hline
{\bf Shell}       &{\bf File}           &{\bf example} \\
\hline
csh               &.cshrc               &setenv EDITOR vi \\
tcsh              &.tcshrc or .cshrc    &setenv PRINTER ps\_rz1 \\
ksh               &.profile             &export VISUAL=emacs  \\
zsh               &.zprofile            &export ORACLE\_SID=v712 \\
bash              &.bash\_profile or     &                    \\
                  & .profile            &export PAGER=less  \\
\hline
\end{tabular}
\end{table}
\end{center}


\begin{htmlonly}
\begin{rawhtml}
<HR>
\end{rawhtml}
\end{htmlonly}

\subsection{How can you customise and understand your X11 environment?}

With version 3.x.x of the HEPiX scripts, an X11 package is provided to you 
and gives you an Xsession default. Under {\verb+$HOME/.hepix+} directory
you will find the following files:
\begin{itemize}
\item \verb+xsession.log+
\item \verb+xprofile+
\item \verb+xclients+
\item \verb+xkeyboard+
\item \verb+xresources+
\end{itemize}

\subsubsection{The {\tt xsession.log} file}

   This file contains the log of the HEP Xsession. When you "{\it Xlogin}"
   the \verb+HEP_Xsession+ program is executed and you can get a trace of its
   execution in the file 
\begin{verbatim}
        $HOME/.hepix/xsession.log
\end{verbatim}

\subsubsection{The {\tt xprofile} file}

   This file is a user configuration file to customise the HEP Xsession.
   This is a bourne shell script which contains typically the setting of
   several "Major Switches". A Major Switch is a variable which controls the
   flow of the Xsession. \\
\\
   Figure \ref{xsession} illustrates the possible behaviours
   you can get from the customisation of the xsession through 

\begin{figure} 
\caption{Model of the {\tt HEP\_Xsession}} \label{xsession}
\epsfig{figure=fig/xsession-light.eps,width=18cm}
\end{figure} 

   Examples are:

   \begin{itemize}
   \item \verb+HX_SOURCEPROFILE+ \\
      This variable can be set to 
      \begin{itemize}
      \item {\tt yes}
      \item {\tt no} (default)
      \end{itemize}
      This variable is used to specify whether the user dot profile files
      should be sourced or not. Indeed, a user might want to get his 
      usual environment (the one he gets in INTERACTIVE or LOGIN states).
      But unfortunately, when the Xsession runs it is not attached to a 
      terminal. Thus it is dangerous to source the user dot files because
      if the user didn't set up his dot profile files correctly, it is possible
      that he tries to "play" with the terminal (by issuing {\bf stty} types
      of commands) which might crash the Xsession.

   \item {\tt HX\_WM} \\
      This variable can be set to
      \begin{itemize}
      \item the word {\tt local}
      \item {\tt full-path/window-manager options}
      \item {\verb+/usr/local/bin/X11/fvwm+} (default)
      \end{itemize}
      This variable is used to specify the window manager the user wants.

   \item {\tt HX\_DESKTOP} \\
      This variable can be set to 
      \begin{itemize}
      \item {\tt HEPIX} (default)
      \item {\tt CDE}
      \item {\tt HP-VUE}
      \item {\tt OPENLOOK}
      \item {\tt DXSESSION}
      \end{itemize}
      This variable is used to specify the Desktop type. Warning not all of the
      Desktop types are available on all platforms. 

   \item {\tt HX\_STARTUP} \\
      This variable can be set to
      \begin{itemize}
      \item {\verb+$HOME/.xsession+}
      \item {\tt \$ETC/X11/hep/HEP\_xsession.default} (default)
      \item Any other executable file
      \end{itemize}
      This variable is used to specify the {\it startup} sequence.

   \item {\tt LAST\_CLIENT} \\
      This variable can be set to
      \begin{itemize}
      \item {\tt window-manager+} (default)
      \item Any other program
      \end{itemize}
      This variable is used to specify the last client also called the {\it 
      control client} of the session.

   \end{itemize}


\underline{Example of {\tt xprofile} file}\\

\begin{verbatim}
 HX_WM=/usr/local/bin/X11/fvwm
 HX_LAST_CLIENT="/usr/bin/X11/xterm -ls -sl 1000 -sb"
 HX_SOURCEPROFILE=yes
 if [ "$OS" = AIX ]; then
    HX_DESKTOP=CDE
 fi
 if [ "$OS" = "HP-UX" ]; then
    HX_DESKTOP=HP-VUE
 fi
 if [ "$OS" = "OSF1" ]; then
    HX_DESKTOP=DXSESSION
 fi
\end{verbatim}

\subsubsection{The {\tt xkeyboard} file}

   This file is used to specify which keyboard name should be used with
the current DISPLAY. \\
\\
\underline{Example of {\tt xkeyboard} file} \\

{\small
\begin{verbatim}
#======================================================================#
# The basic format of this file is:     #
#                                       #
#    Keyboard-name                      # All keyboards map to this one
# or                                    #
#    Display-IP-number  Keyboard-name   # Only the keyboard attached to
# or                                    # Display-IP-number is mapped
#    Display-name       Keyboard-name   # Only the keyboard attached to 
# or                                    # Display-name is mapped
#                                       #-------------#
#                                                     #
#    Display-name|Display-IP-number     Keyboard-name # Keyboard attached
#                                                     # to Display-IP-number
#                                       #-------------# or to Display-name
#                                       # is mapped to Keyboard-name
#----------------------------------------------------------------------
# NCD101
# 128.141.198.155                               NCD102
# mish156.cern.ch                       SGI
# mish157.cern.ch|128.141.198.156       Sun
\end{verbatim}
}

Consult the template xkeyboard file. The {\bf uco} command may help you 
to set a keyboard xmodmap file or see the CERN XTerminal Guide from L. Cons
which contains a few amount of information.

\subsubsection{The {\tt xclients} file}

   This file is used to specify the clients which have to be launched at
   the beginning of the session. Please keep the number of clients small
   when you are working on a multi-user service. Other users want to work
   and launch programs as well!\\
\\
\underline{Example of {\tt xclients} file} \\

\begin{verbatim}
sleep 1; xterm  -geometry 80x24+10+10 -n login -T login -ls -sb -sl 256 &
sleep 1; xclock -geometry 120x120-0+0 -hl Yellow -bg DarkGreen -fg White -hd Red &
\end{verbatim}

Consult the template xclients file which is provided.\\

\subsubsection{The {\tt xresources} file}

   This file is used to specify client resources.

\begin{htmlonly}
\begin{rawhtml}
<HR>
\end{rawhtml}
\end{htmlonly}

\section{User templates}

In each newly created account all the templates files for the customisation of
the shell are provided. You can edit these files using the examples
described in the previous section to customise their environments. \\
\\
In these templates you will find 3 parts:\\
\\
\begin{itemize}
\item[1] the header part which gives the email address in case of serious 
         problems, the version number, the generation date, the authors, etc.\\
\item[2] the hook to the HEPiX scripts. This hook is not required if the HEPiX 
         scripts are installed in enforced mode. You should anyway NEVER 
         remove these 3 lines for the C-shell users
\begin{verbatim}
        if ( -r /usr/local/lib/hepix/central_env.csh ) then
           source /usr/local/lib/hepix/central_env.csh
        endif
\end{verbatim}
        or the following lines for the Bourne-shells users:
\begin{verbatim}
        if [ -r /usr/local/lib/hepix/central_env.sh ]; then
           . /usr/local/lib/hepix/central_env.sh
        fi
\end{verbatim}
         You can find something similar to these lines here.\\
\item[3] commented lines. All these lines can be uncommented. They give
         users examples of what they can do and you can choose now to use them.
         They can use it, remove it, update it as they wish.
\end{itemize}

These templates give other examples and, for {\bf tcsh} or {\bf zsh} shells,
there are other related documents like \cite{taddei.shellsintro}, 
\cite{taddei.tcshcustom}, \cite{taddei.zshcustom}, 
\cite{taddei.zshoptions}.\\

Templates also exist for the {\tt xprofile}, {\tt xclients}  and 
{\tt xkeyboard} files as well.

\begin{htmlonly}
\begin{rawhtml}
<HR>
\end{rawhtml}
\end{htmlonly}

\section{{\tt uco} command}

The {\bf uco} command is a perl program which you can use to reset your 
environment in case it has been corrupted or you have made a mistake in 
customising your  environment. This command can be used to reset your 
shell profiles files, some of your mail default files and some of your 
security files. Type \verb+uco -h+ to get a quick help.\\
\\
In addition, this command can help you to set, reset or analyse some of your customisation.
It can do it on several areas like the shell, X11, the setting of the 
keyboard in the context of X Window, the security, the structure of your 
home directory.\\
\\
It will present you a menu and you can choose what you want to do. You can 
set the flags {\tt -n} to see what the command would do (without
actually performing any operations). The {\tt -v} flag is verbose mode and 
the command informs you what it is doing.
.\\
\\
Of course it saves your files by appending a number
in the file name. For example, assume that it is the first time you run {
\bf uco} on the
shell area to reset your environment
and lets assume that your login shell is {\bf tcsh}, then you will find
{\tt .tcshrc.1} and {\tt .login.1} files in your {\tt HOME} directory. If
it is the 1000th time you run the command you will find 1000 files in your {\tt
HOME} directory, the last ones being: {\tt .tcshrc.1000} and 
{\tt .login.1000}


\begin{htmlonly}
\begin{rawhtml}
<HR>
\end{rawhtml}
\end{htmlonly}


\section{Acknowledgments}

Thanks to 
Roger Jones,
Anne Ryan,
Francesco Giacomini,
Alessandro Miotto and
Alan Silverman
for their feedback and help on this documentation.

\begin{htmlonly}
\begin{rawhtml}
<HR>
\end{rawhtml}
\end{htmlonly}

\nocite{*}
\bibliographystyle{plain}
\bibliography{../common/hepix}



\begin{htmlonly}
\begin{rawhtml}
<HR>
This document had been translated from LaTeX source using the command:
<PRE>
latex2html -split 0 -t "HEPiX Shells and X11 Login Scripts - Implementation at CERN: User Guide" -no_navigation -info "" -show_section_numbers user-guide.tex
</PRE>
<HR>
\end{rawhtml}
\end{htmlonly}







\end{document}



